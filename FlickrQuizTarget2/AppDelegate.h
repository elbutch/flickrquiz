//
//  AppDelegate.h
//  FlickrQuizTarget2
//
//  Created by Mostafa Elbutch on 2/5/16.
//  Copyright (c) 2016 Mostafa Elbutch. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

