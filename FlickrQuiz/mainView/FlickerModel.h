//
//  FlickerModel.h
//  FlickrQuiz
//
//  Created by Mostafa Elbutch on 2/13/16.
//  Copyright (c) 2016 Mostafa Elbutch. All rights reserved.
//

#import "BaseModel.h"

@protocol Photo


@end

@protocol FlickrResponse

@end


@interface Photo : JSONModel
                  
                  
@property (nonatomic,assign) NSNumber <Optional> *key;
@property (nonatomic,assign) int farm;
@property (nonatomic,assign) int id;
@property (nonatomic,assign) int isfamily;
@property (nonatomic,assign) int isfriend;
@property (nonatomic,assign) int ispublic;
@property (nonatomic,strong) NSString *owner;
@property (nonatomic,strong) NSString *secret;
@property (nonatomic,strong) NSString *server;
@property (nonatomic,strong) NSString *title;

@end

@interface FlickrResponse : JSONModel
@property (nonatomic,assign) int page;
@property (nonatomic,assign) int pages;
@property (nonatomic,assign) int perpage;
@property (nonatomic,assign) int total;
@property  (nonatomic,strong) NSArray <Photo> *photo;
@end



@interface FlickerModel : BaseModel

@property (nonatomic,strong) FlickrResponse *photos;

@end
