//
//  OwnerTableViewCell.h
//  FlickrQuiz
//
//  Created by Mostafa Elbutch on 2/8/16.
//  Copyright (c) 2016 Mostafa Elbutch. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FlickerModel.h"
#import "Constants.h"
@interface OwnerTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet UILabel *title;


-(void) customizeCellWithModel:(Photo*)photo;


@end
