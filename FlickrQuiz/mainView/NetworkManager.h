//
//  NetworkManager.h
//  FlickrQuiz
//
//  Created by Mostafa Elbutch on 2/14/16.
//  Copyright (c) 2016 Mostafa Elbutch. All rights reserved.
//



#import <Foundation/Foundation.h>
#import "BaseModel.h"
#import "AFNetworking.h"
#import "FlickerModel.h"





typedef void (^NetworkSuccessBlock)(BaseModel* model);
typedef void (^NetworkFailureBlock)(NSError *error);

@interface NetworkManager : NSObject
@property (nonatomic,strong) FlickerModel *flicker;
@property (nonatomic,strong) NSMutableArray *flickerobjects;

-(void)fetchFlickerDataForText:(NSString*)text WithSuccessBlock:(NetworkSuccessBlock)success failure:(NetworkFailureBlock)failure;

-(void)fetchDataForUser:(NSString*)owner WithSuccessBlock:(NetworkSuccessBlock)success failure:(NetworkFailureBlock)failure;


@end
