//
//  OwnerResultsTableViewController.m
//  FlickrQuiz
//
//  Created by Mostafa Elbutch on 2/14/16.
//  Copyright (c) 2016 Mostafa Elbutch. All rights reserved.
//

#import "OwnerResultsTableViewController.h"
#import "OwnerTableViewCell.h"
#import "NetworkManager.h"


@interface OwnerResultsTableViewController ()
@property (nonatomic,strong) NetworkManager *manager;
@property (nonatomic,strong) FlickerModel *flickerModel;
@end

@implementation OwnerResultsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = _owner;
   
    
    NSString *targetName =[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleName"];
    if ([targetName isEqualToString:@"FlickrQuiz copy"]){
        self.tableView.backgroundColor = [UIColor   grayColor];
    }
    
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
    _manager = [[NetworkManager alloc]init];
    
    [_manager fetchDataForUser:_owner WithSuccessBlock:^(BaseModel *model) {
    
        _flickerModel = (FlickerModel*)model;
        
        [self.tableView reloadData];
        
    } failure:^(NSError *error) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error while requesting data"
                                                            message:[error localizedDescription]
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
        [alertView show];
    }];
        
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        });
    });
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 75.0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return _flickerModel.photos.photo.count;;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    OwnerTableViewCell *cell  = [tableView dequeueReusableCellWithIdentifier:@"ownerCell"];
    if (!cell){
        cell = [[OwnerTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ownerCell"];
        
    }
    
    Photo *photo = [_flickerModel.photos.photo objectAtIndex:indexPath.row];
    
    
//    cell.title.text = photo.title;
    [cell customizeCellWithModel:photo];
    
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
