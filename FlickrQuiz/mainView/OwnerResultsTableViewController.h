//
//  OwnerResultsTableViewController.h
//  FlickrQuiz
//
//  Created by Mostafa Elbutch on 2/14/16.
//  Copyright (c) 2016 Mostafa Elbutch. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"

@interface OwnerResultsTableViewController : UITableViewController

@property (nonatomic,strong) NSString *owner;

@end
