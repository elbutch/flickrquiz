//
//  BaseModel.h
//  FlickrQuiz
//
//  Created by Mostafa Elbutch on 2/13/16.
//  Copyright (c) 2016 Mostafa Elbutch. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface BaseModel : JSONModel

@end
