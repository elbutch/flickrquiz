//
//  SearchResultCustomCellTableViewCell.m
//  FlickrQuiz
//
//  Created by Mostafa Elbutch on 1/30/16.
//  Copyright (c) 2016 Mostafa Elbutch. All rights reserved.
//

#import "SearchResultCustomCellTableViewCell.h"

@implementation SearchResultCustomCellTableViewCell

- (void)awakeFromNib {
    NSString *targetName =[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleName"];
    if ([targetName isEqualToString:@"FlickrQuiz copy"]){
        self.backgroundColor = [UIColor   grayColor];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void) customizeCellWithModel:(Photo*)photo {
    
    self.title.text = photo.title;
    
    NSString *imageURL = [NSString stringWithFormat:FLICKER_IMAGE_URL,photo.farm,photo.server,photo.id,photo.secret];
    
    [self processImageDataWithURLString:imageURL andBlock:^(NSData *imageData) {
        UIImage *image = [UIImage imageWithData:imageData];
        self.image.image = image;

    }];
    
}


- (void)processImageDataWithURLString:(NSString *)urlString andBlock:(void (^)(NSData *imageData))processImage
{
    NSURL *url = [NSURL URLWithString:urlString];
    
    dispatch_queue_t callerQueue = dispatch_get_current_queue();
    dispatch_queue_t downloadQueue = dispatch_queue_create("com.myapp.processsmagequeue", NULL);
    dispatch_async(downloadQueue, ^{
        NSData * imageData = [NSData dataWithContentsOfURL:url];
        
        dispatch_async(callerQueue, ^{
            processImage(imageData);
        });
    });
}

@end
