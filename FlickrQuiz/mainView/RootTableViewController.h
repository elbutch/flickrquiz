//
//  RootTableViewController.h
//  FlickrQuiz
//
//  Created by Mostafa Elbutch on 2/13/16.
//  Copyright (c) 2016 Mostafa Elbutch. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NetworkManager.h"
#import "MBProgressHUD.h"

@interface RootTableViewController : UITableViewController <UISearchBarDelegate>

@end
