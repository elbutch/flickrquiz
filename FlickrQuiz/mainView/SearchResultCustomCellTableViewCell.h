//
//  SearchResultCustomCellTableViewCell.h
//  FlickrQuiz
//
//  Created by Mostafa Elbutch on 1/30/16.
//  Copyright (c) 2016 Mostafa Elbutch. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FlickerModel.h"
#import "Constants.h"

@interface SearchResultCustomCellTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet UILabel *title;


-(void) customizeCellWithModel:(Photo*)photo;

@end
