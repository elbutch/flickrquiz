//
//  NetworkManager.m
//  FlickrQuiz
//
//  Created by Mostafa Elbutch on 2/14/16.
//  Copyright (c) 2016 Mostafa Elbutch. All rights reserved.
//

#import "NetworkManager.h"
#import "Constants.h"
#import "EGOCache.h"
@implementation NetworkManager


-(void)fetchFlickerDataForText:(NSString*)text WithSuccessBlock:(NetworkSuccessBlock)success failure:(NetworkFailureBlock)failure{

    
    
    
    
    NSString *urlString = [NSString stringWithFormat:@"https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=%@&text=%@&format=json&nojsoncallback=1",FLICKR_API_KEY,text];
    NSURL *url = [[NSURL alloc] initWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    AFHTTPRequestOperation *requestOperation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    requestOperation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    
    //requesting the API and parsing JSON file
    [requestOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        
        NSLog(@"%@", responseObject);
        
        
//        NSCache *cache = [[NSCache alloc] init];
//        [cache setObject:responseObject forKey:[NSString stringWithFormat:@"%@",text]];
        
        [[EGOCache globalCache] setObject:responseObject forKey:[NSString stringWithFormat:@"%@",text]];
        
        
        
        _flicker = [[FlickerModel alloc]initWithDictionary:responseObject error:&error];
        
        success(_flicker);
        
        
        
        
        }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"error occured");
        
        }];
     
    [requestOperation start];

    
    
}



-(void)fetchDataForUser:(NSString*)owner WithSuccessBlock:(NetworkSuccessBlock)success failure:(NetworkFailureBlock)failure{
    
    NSString *urlString = [NSString stringWithFormat:@"https://api.flickr.com/services/rest/?method=flickr.people.getPublicPhotos&api_key=%@&user_id=%@&format=json&nojsoncallback=1",FLICKR_API_KEY,owner];
    
    NSURL *url = [[NSURL alloc] initWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    AFHTTPRequestOperation *requestOperation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    requestOperation.responseSerializer = [AFJSONResponseSerializer serializer];
    
    
    //requesting the API and parsing JSON file
    [requestOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *error = nil;
        NSLog(@"%@", responseObject);

        
        _flicker = [[FlickerModel alloc]initWithDictionary:responseObject error:&error];
        
        success(_flicker);
    }failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"error occured");
        
    }];
    
    [requestOperation start];
    
    
}

@end
