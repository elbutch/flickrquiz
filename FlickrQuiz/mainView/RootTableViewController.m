//
//  RootTableViewController.m
//  FlickrQuiz
//
//  Created by Mostafa Elbutch on 2/13/16.
//  Copyright (c) 2016 Mostafa Elbutch. All rights reserved.
//

#import "RootTableViewController.h"
#import "SearchResultCustomCellTableViewCell.h"
#import "OwnerResultsTableViewController.h"
#import "EGOCache.h"

@interface RootTableViewController ()

@property (nonatomic,strong) NetworkManager *manager;
@property (nonatomic,strong) FlickerModel *flickerModel;

@property (nonatomic,strong) NSArray  *filteredData;
@property BOOL isSearching;
@property BOOL didHitSearchButton;
@end

@implementation RootTableViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    
    
   //        self.navigationController.navigationBar.backgroundColor = [UIColor blueColor];
//        self.tableView.backgroundColor = [UIColor yellowColor];

    
    _didHitSearchButton = NO;
    
    NSString *targetName =[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleName"];
    if ([targetName isEqualToString:@"FlickrQuiz copy"]){
        self.tableView.backgroundColor = [UIColor   grayColor];
    }
    
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Hello" message:@"Type to search for an image" delegate:self cancelButtonTitle:@"Ok!" otherButtonTitles:nil];
    [alert show];
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSArray *)filteredData{
    if (!_filteredData)
        _filteredData = [NSArray new];
    return _filteredData;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (_isSearching && _didHitSearchButton){
        return [_filteredData count];
    }
    else if (_didHitSearchButton && !_isSearching){
        return _flickerModel.photos.photo.count;
    }
    else{
        return 0;
    }
    
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    SearchResultCustomCellTableViewCell *cell  = [tableView dequeueReusableCellWithIdentifier:@"flickerCell"];
    if (!cell){
            cell = [[SearchResultCustomCellTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"flickerCell"];
        
    }
    
    if (_isSearching && _didHitSearchButton){
        Photo *photo = [_filteredData objectAtIndex:indexPath.row];
        
        [cell customizeCellWithModel:photo];
    }
    else if (_didHitSearchButton && ! _isSearching){
        Photo *photo = [_flickerModel.photos.photo objectAtIndex:indexPath.row];
        
        [cell customizeCellWithModel:photo];
    }
    
    
    // Configure the cell...
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [self performSegueWithIdentifier:@"toOwnerViewController" sender:indexPath];
    
}


-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    _isSearching = YES;
}
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    if (!searchText.length){
        _isSearching = NO;
        [self.tableView reloadData];
        return;
    }
    
    _isSearching = YES;
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"title CONTAINS[cd] %@",searchText];
    _filteredData = [_flickerModel.photos.photo filteredArrayUsingPredicate:predicate];
    [self.tableView reloadData];
    
}
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    
    //get the initial state of the UISearchBar
//    searchBar.text = @"";
    [searchBar resignFirstResponder];
    [self searchBarCancelButtonClicked:searchBar];
    
    
    
    

    
    
    id responseObject = [[EGOCache globalCache] objectForKey:[NSString stringWithFormat:@"%@",searchBar.text]];
    
    if (responseObject != nil) {
        NSError *error;
        _flickerModel = [[FlickerModel alloc] initWithDictionary:(NSDictionary*)responseObject error:&error];
        _didHitSearchButton = YES;

        [self.tableView reloadData];
    }
    
    else{
    
    _manager = [[NetworkManager alloc]init];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        
        
        
        
    [_manager fetchFlickerDataForText:searchBar.text WithSuccessBlock:^(BaseModel *model) {
        
        _flickerModel = (FlickerModel*)model;
        
        _didHitSearchButton = YES;
        [self.tableView reloadData];

       
    } failure:^(NSError *error) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error while requesting data"
                                                            message:[error localizedDescription]
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
        [alertView show];
    }];
        
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        });
    });
        
    }
    
}
-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    _isSearching = NO;
    [self.tableView reloadData];
}



/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    NSIndexPath *indexPath = (NSIndexPath*)sender;
    
    OwnerResultsTableViewController *ownerVC = [segue destinationViewController];
    if (_isSearching)
        ownerVC.owner = ((Photo*)[_filteredData objectAtIndex:indexPath.row]).owner;
    else
        ownerVC.owner = ((Photo*)[_flickerModel.photos.photo objectAtIndex:indexPath.row]).owner;
    
}


@end
