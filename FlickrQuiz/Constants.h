//
//  Constants.h
//  FlickrQuiz
//
//  Created by Mostafa Elbutch on 2/13/16.
//  Copyright (c) 2016 Mostafa Elbutch. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Constants : NSObject

extern NSString * const FLICKER_IMAGE_URL;
extern NSString * const FLICKR_API_KEY;
@end
